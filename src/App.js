import './app.css';

const App = () => {
  return (
    <>
      <div className="tableWr">
        <table>
          <thead>
            <th>Table Header</th>
          </thead>
          <tbody>
            <tr>
              <td>data</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
};

export default App;
